import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { AppService } from '../app.service';

@Injectable()
export class EmployeeService extends AppService {

  constructor(
    private http: Http
  ) {
    super();
  }

  getEmployee(survey): Observable<any> {
    let getUrl = this.urlPrefix + '/employee/get';
    let data = {
      survey_id : survey.id,
      ids : survey.items.map(function(x){
              return x.id;
            })
    };
    return this.http
      .post(getUrl, data)
      .map(this.extractData)
      .catch(this.handleError);
  }

  getSurveyRecordFromEmployee(survey, employee): String {
    let id = survey.id;

    if (employee.surveys.length > 0) {
      if (this.inThisArray(employee.surveys, 'id', id)) {
        return employee.surveys.filter(function(val) {
          return val['id'] == id;
        })[0];
      }
    }

    return null;
  }
}

import { SweetAlertService } from 'ng2-sweetalert2';
import { Component, OnInit, Input } from '@angular/core';

import { AbstractComponent } from './abstract.component';
import { AuthService } from './auth.service';
import { SurveyService } from './survey/survey.service';
import { EmployeeService } from './employee/employee.service';

declare var jQuery: any;

@Component({
  selector: 'ayala-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent extends AbstractComponent implements OnInit {
  title = 'Ayala App';

  type_paragraph = 'text';
  type_radio = 'radio';
  type_checkbox = 'checkbox';

  survey: any = [];
  employee: any = [];
  items: any = [];

  surveyStatus: any = null;

  activeItem: any;

  @Input() surveyItems;

  constructor (
    private authService: AuthService,
    private surveyService: SurveyService,
    private employeeService: EmployeeService,
    private swalService: SweetAlertService
  ) {
    super();
    this.authService.login()
      .subscribe(
        login => {
          if (!login) {
            window.location.href = this.authService.goLogin();
          }
        }
      )
  }

  ngOnInit(): void {
    // Get the employee Object
    // this.employeeService.getEmployee()
    //   .subscribe(
    //     employee => this.employee = employee
    //   )

    // Get the open survey
    this.surveyService.getSurvey(this.getSurveyID())
      .subscribe(
        survey => {
          this.survey = survey;
          this.items = survey.items;
          this.surveyItems = this.surveyService.makeSurveyItems(survey.items);
          this.employeeService.getEmployee(survey)
            .subscribe(
              employee => {
                this.employee = employee;
                this.surveyStatus = this.employeeService.getSurveyRecordFromEmployee(survey, employee);
                if (this.surveyStatus) {
                  if (this.surveyStatus.pivot.status == 'ongoing') {
                    this.activeItem = this.surveyService.getLastAnsweredItem(this.survey, this.employee);
                  } else {
                    this.activeItem = this.survey.items[0];
                  }
                  this.surveyItems = this.surveyService.makeSurveyItems(survey.items, this.employee.answers);
                  console.log('pasok');
                }
                console.log(this.surveyItems);
              }
            );
        }
      );
  }

  isItemActive(item): boolean {
    if (this.activeItem) {
      return this.activeItem == item;
    }

    return item == this.survey.items[0];
  }

  isItemAnswered(item): boolean {
    let answerRecord = this.surveyItems.filter(function(obj){
      return item['id'] == obj['id'];
    })[0];
    let flag = false;

    if (answerRecord['answer'] instanceof Array) {
      answerRecord['answer'].forEach(element => {
        if (element !== '' && element !== null && element !== false) {
          flag = true;
        }
      });
    } else {
      if (answerRecord['answer'] !== '' && answerRecord['answer'] !== null) {
        flag = true;
      }
    }

    return flag;
  }

  isLastItem(item): boolean {
    let lastItem = this.items[this.items.length - 1];

    if (lastItem == item) {
      return true;
    }

    return false;
  }

  nextItem(item: any, index: number) {
    if (index in this.items) {
      this.activeItem = this.items[index];
    }

    return false;
  }

  changeActiveItem(item: any, index: number) {
    if (index in this.items) {
      this.activeItem = this.items[index];
    }

    return false;
  }

  notFirstItem(item: any): boolean {
    return this.items[0] !== item ? true : false;
  }

  notLastItem(item: any): boolean {
    return this.items[this.items.length - 1] !== item ? true : false;
  }

  radioChange(i, value) {
    this.surveyItems[i].answer = [];
    this.surveyItems[i].answer.push(value);
  }

  getThisRadioSelected(i, e, def): boolean {
    if (this.surveyItems[i].answer.length > 0) {
      return this.surveyItems[i].answer[0] == def ? true : null;
    }

    return null;
  }

  save() {
    this.surveyService
      .save(this.surveyItems, this.survey, this.employee)
      .subscribe(
        response => {
          if (response.success) {
            let _self = this;
            this.successNotification(response.message);
            console.log(this.surveyItems);
          }
        }
      );
  }

  submit() {
    let flag = this.validateItems();
    let _self = this;

    if (flag) {
      this.swalService.confirm({
        title: 'Are you sure?',
        text: 'Take time to review your answers.',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, send it!',
        cancelButtonText: 'No, let me review!',
        confirmButtonClass: 'btn btn-ayala btn-green  btn-small btn-wide',
        cancelButtonClass: 'btn btn-ayala btn-orange btn-small btn-wide',
        buttonsStyling: false
      }).then(function () {
        _self.goSubmit();
        jQuery('.swal2-container').remove();
      }, function (dismiss) {
        jQuery('.swal2-container').remove();
      });
    }
  }

  goSubmit() {
    this.surveyService
      .submit(this.surveyItems, this.survey, this.employee)
      .subscribe(
        response => {
          if (response.success) {
            let _self = this;
            this.successNotification(response.message);
            setTimeout(function(){
              _self.surveyService.goThankYou(_self.survey);
            }, 1000);
          }
        }
      );
  }

  validateItems() {
    let flag = true,
        firstUnaswered = null,
        _self = this;

    this.items.forEach(function(element, index){
      let someflag = _self.isItemAnswered(element);
      if (flag && !someflag) {
        firstUnaswered = element;
        flag = someflag;
      }
    });

    if (!flag) {
      this.errorNotification('All questions are needed to be answered before submission.');
      this.activeItem = firstUnaswered;
    }

    return flag;
  }

}

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { AppService } from '../app.service';

@Injectable()
export class SurveyService extends AppService {

  constructor(
    private http: Http,
  ) {
    super();
  }

  getSurvey(id): Observable<any> {
    let getUrl = this.urlPrefix + '/survey/' + id + '/get';
    return this.http
      .get(getUrl)
      .map(this.extractData)
      .catch(this.handleError);
  }

  getLastAnsweredItem(survey, employee): any {
    survey.items.filter(element => function(){
      let answered = employee.answers.filter(answer => function(){
        if (answer.answer != null) {
          return true;
        }

        return false;
      });
      let lastItemAnswered = answered[answered.length - 1];

      return element['id'] == lastItemAnswered['id'];
    });
    survey = survey.items[survey.items.length - 1];

    return survey;
  }

  makeSurveyItems(items, answers = null): any {
    let data = [];

    items.forEach((element, index) => {
      let someData = {
        answer_id : null,
        id : element.id,
        type : element.type,
        question : element.question,
        answer : element.type === 'text' ? null : []
      };

      if (element.type !== 'text') {
        element.choices.forEach((inElement, inIndex) => {
          someData.answer[inIndex] = false;
        });
      }

      if (answers !== null) {
        let answer = this.findInThisAnswers(answers, element.id);
        someData.answer_id = answer.id;

        if (answer) {
          someData.answer = answer.answer;
        }
      }

      data.push(someData);
    });

    return data;
  }

  findInThisAnswers(answers, id) {
    let answer = answers.filter(function(value) {
      return value.survey_item_id == id ? value : false;
    })[0];

    return answer ? answer : false;
  }

  save(items, survey, employee) {
    let data = {
      survey : survey.id,
      items : items,
      employee : employee.id
    }
    let url = this.urlPrefix + '/survey/save';

    return this.http
      .post(url, data)
      .map(this.extractData)
      .catch(this.handleError);
  }

  submit(items, survey, employee) {
    let data = {
      survey : survey.id,
      items : items,
      employee : employee.id
    }
    let url = this.urlPrefix + '/survey/submit';

    return this.http
      .post(url, data)
      .map(this.extractData)
      .catch(this.handleError);
  }

  goThankYou(survey) {
    location.href = '/account/survey/' + survey.slug + '/thank-you';
  }
}

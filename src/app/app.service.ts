import { Injectable } from '@angular/core';
import { Headers, Response, Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import './rxjs-operators';

@Injectable()
export abstract class AppService {

  public urlPrefix = '/api'; // For local host copy
  public headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});

  public extractData(res: Response) {
    const body = res.json();
    return body || [];
  }

  public handleError(error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    const errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';

    // check for 401 status code
    // if (error.status === 401) {
    //     this.sessionTimeout.next(error.status);
    // }

    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }

  public handlePromiseError(error: any) {
    let messages: any;
    let body = error.json();
    body = body.data || body || {};

    // check for 401 status code
    // if (error.status === 401) {
    //     this.sessionTimeout.next(error.status);
    // }

    if (body.hasOwnProperty('messages')) {
      messages = body.messages;
      messages = Object.keys(messages).map(function(k) { return messages[k][0] });
    } else {
      let errMsg = (error.message) ? error.message : (error.status ? `${error.status} - ${error.statusText}` : 'Server error');
      messages = [errMsg];
    }
    return Promise.reject(messages[0]);
  }

  public inThisArray(array, key, value): boolean {
    let flag = false;

    array.forEach(element => {
      if (element.hasOwnProperty(key)) {
        flag = (element[key] == value) ? true : flag;
      }
    });

    return flag;
  }
}

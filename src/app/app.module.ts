import { BrowserModule } from '@angular/platform-browser';
import { APP_BASE_HREF } from '@angular/common';
import { Http } from '@angular/http';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';

import { AuthenticatedHttpService } from './authenticated-http.service';

import { SurveyService } from './survey/survey.service';
import { EmployeeService } from './employee/employee.service';
import { AuthService } from './auth.service';
import { SweetAlertService } from 'ng2-sweetalert2';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule
  ],
  providers: [
    AuthService,
    SurveyService,
    EmployeeService,
    {
      provide: APP_BASE_HREF,
      useValue: '<%= APP_BASE %>'
    },
    {
      provide: Http,
      useClass: AuthenticatedHttpService
    },
    SweetAlertService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  static get parameters() {
    return [[SweetAlertService]];
  }
}

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { AppService } from './app.service';

@Injectable()
export class AuthService extends AppService {

  userData: any = { name: '' };
  redirectUrl: string;
  isLoggedIn: boolean = false;

  private url = this.urlPrefix + '/auth';

  constructor(private http: Http) {
    super();
  }

  login(): Observable<boolean> {
    return this.http.get(`${this.url}/user`)
    .map(this.extractData)
    .map((res) => {
      if (res.id) {
        this.userData = res;
        localStorage.setItem('auth_token', res.email);
        this.isLoggedIn = true;
      }
      return this.isLoggedIn;
    });
  }

  logout(): Observable<any> {
    return this.http.get(`${this.url}/logout`)
    .map(this.extractData)
    .map((res) => {
      this.isLoggedIn = false;
      return res;
    });
  }

  authorize(creds: any): Promise<any> {
    creds.from_pos = true;
    return this.http.post(`${this.url}/authorize`, creds)
    .toPromise()
    .then(this.extractData)
    .catch(this.handlePromiseError);
  }

  goLogin(): string {
    return this.urlPrefix + '/auth/login';
  }
}

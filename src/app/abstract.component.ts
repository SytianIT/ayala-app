import { Component } from '@angular/core';

declare var jQuery: any;

export abstract class AbstractComponent {

  getSurveyID() {
    return window['surveyId'];
  }

  successNotification(message: string) {
    jQuery.jGrowl(message, {
      theme: 'bg-green',
      position: 'top-right',
      life: 3000,
      click: function(e: any, m: any, o: any) {
        // close notification
        // console.log(e, m, o, 'notification click');
      }
    });
  }

  errorNotification(message: string) {
    jQuery.jGrowl(message, {
      theme: 'bg-red',
      position: 'top-right',
      life: 5000
    });
    this.forceUpdate();
  }

  forceUpdate() {
    setTimeout(() => {
      document.getElementById('ngApp').click();
    });
  }
}

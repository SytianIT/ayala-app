import { AyalaAppPage } from './app.po';

describe('ayala-app App', () => {
  let page: AyalaAppPage;

  beforeEach(() => {
    page = new AyalaAppPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
